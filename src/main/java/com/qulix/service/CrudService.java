package com.qulix.service;

import java.util.List;

public interface CrudService<Entity, Key> {

    Entity create(Entity entity);

    List<Entity> readAll();

    Entity readOne(Key key);

    Entity update(Entity entity, Key key);

    void delete(Key key);
}
