package com.qulix.service;

import com.qulix.entity.Employee;

public interface EmployeeService extends CrudService<Employee, Long> {
}
