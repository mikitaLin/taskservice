package com.qulix.service.implementation;

import com.qulix.entity.Task;
import com.qulix.repository.TaskRepository;
import com.qulix.service.TaskService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public List<Task> readAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task readOne(Long id) {
        return taskRepository.getOne(id);
    }

    @Override
    public Task update(Task updatedTask, Long id) {
        return taskRepository.findById(id)
                .map(task -> {
                    task.setName(updatedTask.getName());
                    task.setWork(updatedTask.getWork());
                    task.setStartDate(updatedTask.getStartDate());
                    task.setEndDate(updatedTask.getEndDate());
                    task.setStatus(updatedTask.getStatus());
                    task.setEmployee(updatedTask.getEmployee());
                    task.setProject(updatedTask.getProject());
                    return task;
                })
                .orElseThrow(() -> new RuntimeException("NOT FOUND"));
    }

    @Override
    public void delete(Long id) {
        taskRepository.deleteById(id);
    }
}
