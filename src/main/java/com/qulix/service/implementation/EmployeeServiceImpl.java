package com.qulix.service.implementation;

import com.qulix.entity.Employee;
import com.qulix.repository.EmployeeRepository;
import com.qulix.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl (EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee create(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> readAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee readOne(Long id) {
        return employeeRepository.getOne(id);
    }

    @Override
    public Employee update(Employee updatedEmployee, Long id) {
        return employeeRepository.findById(id)
                .map(employee -> {
                    employee.setSurname(updatedEmployee.getSurname());
                    employee.setName(updatedEmployee.getName());
                    employee.setPatronymic(updatedEmployee.getPatronymic());
                    employee.setPosition(updatedEmployee.getPosition());
                    employee.setTaskList(updatedEmployee.getTaskList());
                    return employee;
                })
                .orElseThrow(() -> new RuntimeException("NOT FOUND"));
    }

    @Override
    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }
}
