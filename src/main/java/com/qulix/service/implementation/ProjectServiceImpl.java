package com.qulix.service.implementation;

import com.qulix.entity.Project;
import com.qulix.repository.ProjectRepository;
import com.qulix.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }


    @Override
    public Project create(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public List<Project> readAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project readOne(Long id) {
        return projectRepository.getOne(id);
    }

    @Override
    public Project update(Project updatedProject, Long id) {
        return projectRepository.findById(id)
                .map(project -> {
                    project.setName(updatedProject.getName());
                    project.setAbbreviation(updatedProject.getAbbreviation());
                    project.setDescription(updatedProject.getDescription());
                    project.setTaskList(updatedProject.getTaskList());
                    return project;
                })
                .orElseThrow(() -> new RuntimeException("NOT FOUND"));
    }

    @Override
    public void delete(Long id) {
        projectRepository.deleteById(id);
    }
}
