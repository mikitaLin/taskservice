package com.qulix.service;

import com.qulix.entity.Task;

public interface TaskService extends CrudService<Task, Long> {
}
