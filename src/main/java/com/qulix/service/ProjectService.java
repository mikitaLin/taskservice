package com.qulix.service;

import com.qulix.entity.Project;

public interface ProjectService extends CrudService<Project, Long> {
}
