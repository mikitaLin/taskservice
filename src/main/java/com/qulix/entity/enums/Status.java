package com.qulix.entity.enums;

public enum Status {

    NOT_STARTED, IN_PROGRESS, COMPLETED, POSTPONED
}
